local levity = require "levity"

local Camera = class()
Camera.Speed = 800
Camera.FootstepInterval = 10/60

local Sounds = {
	Step1 = "snd/step1.wav",
	Step2 = "snd/step2.wav",
}
levity.bank:load(Sounds)

function Camera:_init(object)
	self.body = object.body
	self.destX, self.destY = self.body:getWorldCenter()
	self.footstepTimer = nil
end

function Camera:playerMoved(newX, newY)
	local roomSizeHalf = levity.map.properties.roomSize/2
	self.destX, self.destY = newX - roomSizeHalf, newY - roomSizeHalf

	local X, Y = self.body:getPosition()
	local distX, distY = self.destX - X, self.destY - Y
	local dist = math.hypot(distX, distY)
	local velX, velY = Camera.Speed*distX/dist, Camera.Speed*distY/dist
	self.body:setLinearVelocity(velX, velY)
	self.footstepTimer = 0
	levity.bank:play(Sounds["Step"..love.math.random(2)])
end

function Camera:endMove(dt)
	if self.footstepTimer then
		if math.floor(self.footstepTimer/Camera.FootstepInterval) <
		math.floor((self.footstepTimer + dt)/Camera.FootstepInterval)
		then
			levity.bank:play(Sounds["Step"..love.math.random(2)])
		end
		self.footstepTimer = self.footstepTimer + dt
	end
	local velX, velY = self.body:getLinearVelocity()
	local X, Y = self.body:getPosition()
	local toDestX = self.destX - X
	local toDestY = self.destY - Y
	if math.dot(velX, velY, toDestX, toDestY) < 0 then
		self.body:setPosition(self.destX, self.destY)
		self.body:setLinearVelocity(0, 0)
		self.footstepTimer = nil
	end
	levity.camera:set(self.body:getWorldCenter())
	local inventory = levity.map.layers["inventory"]
	inventory.x, inventory.y = self.body:getPosition()
end

return Camera
