local levity = require "levity"

local Sounds = {
	GetItem = "snd/getitem.wav",
	No = "snd/no.wav",
	BreakWall = "snd/breakwall.wav"
}
levity.bank:load(Sounds)

local Player = class()
function Player:_init(object)
	self.object = object
	self.body = object.body
	local roomSize = levity.map.properties.roomSize
	levity.camera:set(object.x, object.y, roomSize, roomSize)

	self.items = {}
end

function Player:mousereleased(x, y)
	self:touchreleased(nil, x, y)
end

function Player:touchreleased(touch, x, y)
	x, y = levity:screenToCamera(x, y)

	local vx, vy = x - levity.camera.w/2, y - levity.camera.h/2
	local dotX = math.dot(1, 0, vx, vy)
	local dotY = math.dot(0, 1, vx, vy)
	local absDotX = math.abs(dotX)
	local absDotY = math.abs(dotY)
	if absDotX > absDotY then -- left or right
		self:commandMove(dotX/absDotX, 0)
	else -- up or down
		self:commandMove(0, dotY/absDotY)
	end
end

function Player:commandMove(dirX, dirY)
	self:faceAngle(math.atan2(dirY, dirX))

	local x, y = self.body:getPosition()
	local roomSize = levity.map.properties.roomSize
	local newX, newY = x + roomSize*dirX, y + roomSize*dirY

	-- not out of the map
	if levity.scripts:call(levity.mapfile, "isOutOfBounds", newX, newY) then
		return
	end

	-- only if something interesting that way
	local col, row = levity.scripts:call(levity.mapfile, "findInteractable",
						x, y, dirX, dirY)
	if not col or not row then
		return
	end

	-- if item required, check for it
	local interactItem = levity.scripts:call(levity.mapfile, "getInteractItem", col, row)

	if interactItem then
		local correctItem
		for _, item in pairs(self.items) do
			if item == interactItem then
				correctItem = item
				break
			end
		end
		if not correctItem then
			levity.bank:play(Sounds.No)
			return
		end
	end

	-- if item, get it
	local item = levity.scripts:call(levity.mapfile, "getItemName", col, row)
	if item then
		levity.bank:play(Sounds.GetItem)
		self.items[#self.items + 1] = item
		levity.scripts:call(levity.mapfile, "setInteractedTile", col, row)
		levity.map:setLayerTile("inventory", #self.items, 1,
					levity.map:getTileGid("items", item))
		return
	end

	-- if exit closed, open it first
	local exitOpen = levity.scripts:call(levity.mapfile, "isExitOpen",
						col, row)
	if exitOpen == false then
		levity.scripts:call(levity.mapfile, "openExitTiles",
					x, y, dirX, dirY)
		levity.bank:play(Sounds.BreakWall)
		return
	end

	self.body:setPosition(newX, newY)
	levity.scripts:broadcast("playerMoved", newX, newY)
end

local faceAngle_tile = {
	direction = nil
}

function Player:faceAngle(angle)
	local tilesetProperties = levity.map.tilesets[self.object.tile.tileset].properties
	local numDirections = tilesetProperties.numDirections
	if not numDirections then
		return
	end

	local directionarc = 2*math.pi/numDirections
	local i = math.floor(angle/directionarc + .5) % numDirections
	faceAngle_tile.direction = i

	local gid = levity.map:getTileGid(self.object.tile.tileset, faceAngle_tile)
	if gid ~= self.object.gid then
		self.object:setGid(gid, levity.map)
	end
end

return Player
