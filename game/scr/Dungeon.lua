local levity = require "levity"

local Dungeon = class()
function Dungeon:_init(map)
	self.map = map
end

--@param x in pixels
--@param y in pixels
--@return If point (x, y) is out of map bounds
function Dungeon:isOutOfBounds(x, y)
	local col, row = self.map:convertPixelToTile(x, y)
	return col < 0 or col >= levity.map.width
		or row < 0 or row >= levity.map.height
end

--- Search for something interesting a short distance from given point
--@param fromX in pixels
--@param fromY in pixels
--@param dirX -1, 0, or 1
--@param dirY -1, 0, or 1
--@return Found cell (column, row)
function Dungeon:findInteractable(fromX, fromY, dirX, dirY)
	local roomSize = self.map.properties.roomSize
	local fromCol, fromRow = self.map:convertPixelToTile(fromX, fromY)
	local searchCols, searchRows = self.map:convertPixelToTile(
					dirX*roomSize/2, dirY*roomSize/2)
	local col, row
	for r = fromRow, fromRow + searchRows, dirY do
		for c = fromCol, fromCol + searchCols, dirX do
			local tileprop = self.map:getTileProperties("interactable", c, r)

			if tileprop.exitOpen ~= nil
			or tileprop.itemName
			then
				col, row = c, r
				break
			end
			if dirX == 0 then break end
		end
		if dirY == 0 then break end
	end
	return col, row
end

function Dungeon:getItemName(col, row)
	local tileprop = self.map:getTileProperties("interactable", col, row)
	return tileprop.itemName
end

function Dungeon:getInteractItem(col, row)
	local tileprop = self.map:getTileProperties("interactable", col, row)
	return tileprop.interactItem
end

function Dungeon:isExitOpen(col, row)
	local tileprop = self.map:getTileProperties("interactable", col, row)
	return tileprop.exitOpen
end

function Dungeon:openExitTiles(fromX, fromY, dirX, dirY)
	local roomSize = self.map.properties.roomSize
	local fromCol, fromRow = self.map:convertPixelToTile(fromX, fromY)
	local searchCols, searchRows = self.map:convertPixelToTile(
					dirX*roomSize, dirY*roomSize)

	for r = fromRow, fromRow + searchRows, dirY do
		for c = fromCol, fromCol + searchCols, dirX do
			if self:isExitOpen(c, r) == false then
				self:setInteractedTile(c, r)
			end
			if dirX == 0 then break end
		end
		if dirY == 0 then break end
	end
end

function Dungeon:setInteractedTile(col, row)
	local layer = self.map.layers["interactable"]
	local tile = layer.data[row][col]
	local tileset = self.map.tilesets[tile.tileset]
	local interactedTile = tile.properties.interactedTile
	local interactedGid = interactedTile and tileset.firstgid + interactedTile
		or nil
	self.map:setLayerTile("interactable", col, row, interactedGid)
end

function Dungeon:keypressed_escape()
	love.event.quit()
end

return Dungeon
