return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "1.0.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 50,
  height = 50,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 6,
  properties = {
    ["roomSize"] = 160,
    ["script"] = "Dungeon"
  },
  tilesets = {
    {
      name = "tileset",
      firstgid = 1,
      filename = "img/tileset.tsx",
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "img/tileset.png",
      imagewidth = 448,
      imageheight = 848,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 1484,
      tiles = {
        {
          id = 687,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 688,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 689,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 690,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 691,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 692,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 694,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 695,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 696,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 697,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 698,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 699,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 715,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 716,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 717,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 718,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 719,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 720,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 722,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 723,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 724,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 725,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 726,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 727,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 743,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 744,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 745,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 746,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 747,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 748,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 750,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 751,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 752,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 753,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 754,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 755,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 771,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 772,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 773,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 774,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 775,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 776,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 778,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 779,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 780,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 781,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 782,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 783,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 799,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 800,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 801,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 802,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 803,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 804,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 806,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 807,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 808,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 809,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 810,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 811,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 827,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 828,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 829,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 830,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 831,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 832,
          properties = {
            ["exitAngle"] = 270,
            ["exitOpen"] = true
          }
        },
        {
          id = 834,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 835,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 836,
          properties = {
            ["exitAngle"] = 180,
            ["exitOpen"] = true
          }
        },
        {
          id = 837,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 838,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 839,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 855,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 856,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 857,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 858,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 859,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 860,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 862,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 863,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 864,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 865,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 866,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 867,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 883,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 884,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 885,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 886,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 887,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 888,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 890,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 891,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 892,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 893,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 894,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 895,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 911,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 912,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 913,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 914,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 915,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 916,
          properties = {
            ["exitAngle"] = 90,
            ["exitOpen"] = true
          }
        },
        {
          id = 918,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 919,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 920,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 921,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 922,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 923,
          properties = {
            ["exitAngle"] = 0,
            ["exitOpen"] = true
          }
        },
        {
          id = 980,
          probability = 0.05
        },
        {
          id = 1008,
          probability = 0.05
        },
        {
          id = 1282,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1283,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1282
          }
        },
        {
          id = 1310,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1311,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1310
          }
        },
        {
          id = 1338,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1339,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1338
          }
        },
        {
          id = 1364,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1365,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1366,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1367,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1368,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1369,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1384,
          animation = {
            {
              tileid = 1384,
              duration = 100
            },
            {
              tileid = 1385,
              duration = 100
            },
            {
              tileid = 1386,
              duration = 100
            },
            {
              tileid = 1387,
              duration = 100
            },
            {
              tileid = 1388,
              duration = 100
            },
            {
              tileid = 1389,
              duration = 100
            },
            {
              tileid = 1390,
              duration = 100
            },
            {
              tileid = 1391,
              duration = 100
            }
          }
        },
        {
          id = 1392,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1364
          }
        },
        {
          id = 1393,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1365
          }
        },
        {
          id = 1394,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1366
          }
        },
        {
          id = 1395,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1367
          }
        },
        {
          id = 1396,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1368
          }
        },
        {
          id = 1397,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1369
          }
        },
        {
          id = 1422,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1423
          }
        },
        {
          id = 1423,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1450,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1451
          }
        },
        {
          id = 1451,
          properties = {
            ["exitOpen"] = true
          }
        },
        {
          id = 1478,
          properties = {
            ["exitOpen"] = false,
            ["interactItem"] = "Hammer",
            ["interactedTile"] = 1479
          }
        },
        {
          id = 1479,
          properties = {
            ["exitOpen"] = true
          }
        }
      }
    },
    {
      name = "sara",
      firstgid = 1485,
      filename = "img/sara.tsx",
      tilewidth = 16,
      tileheight = 18,
      spacing = 0,
      margin = 0,
      image = "img/sara.png",
      imagewidth = 144,
      imageheight = 72,
      tileoffset = {
        x = -8,
        y = 9
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 18
      },
      properties = {
        ["direction_east"] = 0,
        ["numDirections"] = 4,
        ["rowstype"] = "direction"
      },
      terrains = {},
      tilecount = 36,
      tiles = {
        {
          id = 0,
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            }
          }
        },
        {
          id = 9,
          animation = {
            {
              tileid = 9,
              duration = 100
            },
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 10,
              duration = 100
            }
          }
        },
        {
          id = 18,
          animation = {
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            },
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 27,
          animation = {
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            }
          }
        }
      }
    },
    {
      name = "items",
      firstgid = 1521,
      filename = "img/items.tsx",
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "img/items.png",
      imagewidth = 208,
      imageheight = 240,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 195,
      tiles = {
        {
          id = 113,
          properties = {
            ["itemName"] = "Hammer",
            ["name"] = "Hammer"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "rooms",
      x = 0,
      y = 0,
      width = 50,
      height = 50,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt2kdMVGEQwPHvrYsidlHBjkZRsUAUFRUbdsXeAIGoB2zEXsB2sCdiQRGsFw9GDSgWUO8W7IB6sWKNNI+A3f8ed4zx20R8j+RN8jvwMTuZ4ZE3+ZJ9qpR6hud4IbzEKxRhhkOpmZiF2UIUohGDFuT6wR8thVZojTbIMJQ6jCM4KhzDcZyAblSgElX4LHzBV3zDcnpcgZVYJazGGqxFb3L7IBR9hX7ojzBcosfLyEGucAVXcc2DOZqS64tmaC60gB/8kUKPe7AX+4T9SMUBTKLmZEzBVGEaprueLR5SMx8FKBQe4TGeeDBHL3KDEYIwp7venPVBKE7T4xmcxRCnu0zOsnAOi6m5BIlIEJZimevZopSaZSh3/e2d7io4q0SVPYc9h8lzjCd3AiIxUZiEyZiC6/R4AzdxS8jDbdzBDmruxBA+M1QYht38LgUNyW2ExmgiNIUvmjn050ig9gIsxCJhMZYgEUXUfI03eCu8w3t8wElqRpMfgzlCLOJcz5acIHK7owd6Cr0QjBAP5thC3a3Yhu3CDuzELnyj5nf8wE9B1VLKgAOryV2DtVgnJCEZ6zGaz43BWIwTxmMCIj2Y41/HPdzHAzwU8lGAQtO604+PKEYJSoUylOOTad3ph5P/GS/URh3BG3Xh48F7yKwIoMcO6Gj8/t7txFlnBNpz/Lew57BWhNPjYOPP+3w4ImrAHFHG3/d5fA2YQ3efmxW6e9qsPN3Q3dNm5emG7p42K083dN+nZuXZc9hzmDmH7p42K083dPe0WXm6obunzcqz+p72Rl34oJ5QHw3QUFl/T3dCZwSii9AV3RCkrL+nh9HjcERghDASozBaWf99GsvPcYjHKae7uZzNw3x7DnuOaprD6ns6iR6TsR4bhI3YhM3K+ns6jR4PIR0ZwmEcwVFl/T2dTY8XcBGXhMvIQa7Sj/3UTMUBHBTScAjphvXv55n0mIVzOC9k4wIuGta/n+fR423cwV3hHu7jgWH9+/k7ct/jAxZ5ufvIWTFKDOvfo+w57DmqYw7lUMqAA7UEJ7xQ22H9+3kbemyLdmgvBKADOjqsfz8Po8cBGIhBQjgGu76v4TBv7/8CVC/rEA=="
    },
    {
      type = "tilelayer",
      name = "backdecor",
      x = 0,
      y = 0,
      width = 50,
      height = 50,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt0DEKwkAQBdApNleIt47gOcTzCAa9RCBiGqeRVFqtCSHvwW//n90Itu5WIvpSt/OafY/MvXLvL2NuPSvvDdk3ZV4LvuNfDk1E26x9BQAAAN+cypwtu5Q5tXTZdc4cF/ybzyYAAAAAsC9vWiUQcw=="
    },
    {
      type = "tilelayer",
      name = "interactable",
      x = 0,
      y = 0,
      width = 50,
      height = 50,
      visible = true,
      opacity = 1,
      offsetx = 8,
      offsety = 8,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztl0lKA0EUhpvuJCqOR4gTjkeIE45HcNxEL6AmUTfqDdSoG/UGbhxRT+DGEfUOjqg38AN7k6B0EV46XU398FFU16Pr/R3IX2VZRkb+qcaxrFqoc37nlYxVUO3k1iWZT8OM+/zMtqxzuLALq5NWO/t1QKe7bwtjK7Tl+VhmvgKr7vMH+nqEJ7uwumRM1scQ+w3DiLtvP+MADOb52GG+C3vu80/6+oJvu7C6oGotWuoOZLQfEh+XIfERFkVC8nvEQ+IjERIfRkbSGuUMMgbjjndtkJWi/zRkNPexTv8bkNXcR1h0zJn8BE41OZv/p1v6v4N7zX280f87fGjgI+j3c1VJ389TnMfSkHHPZbOMczBf5HOazvdz1ZyWrpOWak5L10lLNael64z+lmpOS9dJSzWnpeukpZrT0nXFVNBzWjVXVXM66ApiTqsqQs9RiPmc5xOsT8KUUM7EeU89NPic5wusL8KSkI8E7+mCbp/zfJP1Ldg2uZ+jMr5HOVR4fBfpPD9g/RCOhP5DGum/CZo9fEjn+RXr13Aj5KOH/nuhz8OHdJ4/s/4CrxrkftDPB9L3+FJJ53u8kZHu+gFEe7Qw"
    },
    {
      type = "objectgroup",
      name = "player",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "player",
          type = "Player",
          shape = "rectangle",
          x = 80,
          y = 80,
          width = 16,
          height = 18,
          rotation = 0,
          gid = 1494,
          visible = true,
          properties = {}
        },
        {
          id = 3,
          name = "camera",
          type = "Camera",
          shape = "rectangle",
          x = 0,
          y = 0,
          width = 160,
          height = 160,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "tilelayer",
      name = "frontdecor",
      x = 0,
      y = 0,
      width = 50,
      height = 50,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztz0EJACAUBcF3+P3zaQ+9awdBQWYS7CYA97RKer2uODf2w/zgAwAAAAAAAADgJwtySwOr"
    },
    {
      type = "tilelayer",
      name = "inventory",
      x = 0,
      y = 0,
      width = 50,
      height = 50,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJztwQENAAAAwqD3T20ON6AAAAAAAAAAAADg3wAnEAAB"
    },
    {
      type = "objectgroup",
      name = "help",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 2,
          name = "welcome",
          type = "",
          shape = "rectangle",
          x = 16,
          y = 120,
          width = 128,
          height = 32,
          rotation = 0,
          visible = true,
          properties = {
            ["text"] = "TAP DOOR TO WALK THROUGH",
            ["textalign"] = "left",
            ["textfont"] = "fnt/Press Start 2P.fnt"
          }
        },
        {
          id = 5,
          name = "getitem",
          type = "",
          shape = "rectangle",
          x = 496,
          y = 120,
          width = 128,
          height = 32,
          rotation = 0,
          visible = true,
          properties = {
            ["text"] = "TAP ITEM TO PICK IT UP",
            ["textalign"] = "left",
            ["textfont"] = "fnt/Press Start 2P.fnt"
          }
        },
        {
          id = 4,
          name = "hint",
          type = "",
          shape = "rectangle",
          x = 336,
          y = 16,
          width = 128,
          height = 32,
          rotation = 0,
          visible = true,
          properties = {
            ["text"] = "FIND AN ITEM TO BREAK THROUGH THE WALL",
            ["textalign"] = "left",
            ["textfont"] = "fnt/Press Start 2P.fnt"
          }
        }
      }
    }
  }
}
