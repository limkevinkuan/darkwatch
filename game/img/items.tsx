<?xml version="1.0" encoding="UTF-8"?>
<tileset name="items" tilewidth="16" tileheight="16" tilecount="195" columns="13">
 <image source="items.png" width="208" height="240"/>
 <tile id="113">
  <properties>
   <property name="itemName" value="Hammer"/>
   <property name="name" value="Hammer"/>
  </properties>
 </tile>
</tileset>
