<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tileset" tilewidth="16" tileheight="16" tilecount="1484" columns="28">
 <image source="tileset.png" width="448" height="848"/>
 <tile id="687">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="688">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="689">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="690">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="691">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="692">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="694">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="695">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="696">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="697">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="698">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="699">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="715">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="716">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="717">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="718">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="719">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="720">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="722">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="723">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="724">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="725">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="726">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="727">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="743">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="744">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="745">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="746">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="747">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="748">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="750">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="751">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="752">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="753">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="754">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="755">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="771">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="772">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="773">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="774">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="775">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="776">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="778">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="779">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="780">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="781">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="782">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="783">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="799">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="800">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="801">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="802">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="803">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="804">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="806">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="807">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="808">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="809">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="810">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="811">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="827">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="828">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="829">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="830">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="831">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="832">
  <properties>
   <property name="exitAngle" type="float" value="270"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="834">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="835">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="836">
  <properties>
   <property name="exitAngle" type="float" value="180"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="837">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="838">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="839">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="855">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="856">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="857">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="858">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="859">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="860">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="862">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="863">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="864">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="865">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="866">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="867">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="883">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="884">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="885">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="886">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="887">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="888">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="890">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="891">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="892">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="893">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="894">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="895">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="911">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="912">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="913">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="914">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="915">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="916">
  <properties>
   <property name="exitAngle" type="float" value="90"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="918">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="919">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="920">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="921">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="922">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="923">
  <properties>
   <property name="exitAngle" type="float" value="0"/>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="980" probability="0.05"/>
 <tile id="1008" probability="0.05"/>
 <tile id="1282">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1283">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1282"/>
  </properties>
 </tile>
 <tile id="1310">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1311">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1310"/>
  </properties>
 </tile>
 <tile id="1338">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1339">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1338"/>
  </properties>
 </tile>
 <tile id="1364">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1365">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1366">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1367">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1368">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1369">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1384">
  <animation>
   <frame tileid="1384" duration="100"/>
   <frame tileid="1385" duration="100"/>
   <frame tileid="1386" duration="100"/>
   <frame tileid="1387" duration="100"/>
   <frame tileid="1388" duration="100"/>
   <frame tileid="1389" duration="100"/>
   <frame tileid="1390" duration="100"/>
   <frame tileid="1391" duration="100"/>
  </animation>
 </tile>
 <tile id="1392">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1364"/>
  </properties>
 </tile>
 <tile id="1393">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1365"/>
  </properties>
 </tile>
 <tile id="1394">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1366"/>
  </properties>
 </tile>
 <tile id="1395">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1367"/>
  </properties>
 </tile>
 <tile id="1396">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1368"/>
  </properties>
 </tile>
 <tile id="1397">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1369"/>
  </properties>
 </tile>
 <tile id="1422">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1423"/>
  </properties>
 </tile>
 <tile id="1423">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1450">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1451"/>
  </properties>
 </tile>
 <tile id="1451">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1478">
  <properties>
   <property name="exitOpen" type="bool" value="false"/>
   <property name="interactItem" value="Hammer"/>
   <property name="interactedTile" type="int" value="1479"/>
  </properties>
 </tile>
 <tile id="1479">
  <properties>
   <property name="exitOpen" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
