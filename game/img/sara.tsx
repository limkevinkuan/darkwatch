<?xml version="1.0" encoding="UTF-8"?>
<tileset name="sara" tilewidth="16" tileheight="18" tilecount="36" columns="9">
 <tileoffset x="-8" y="9"/>
 <properties>
  <property name="direction_east" type="int" value="0"/>
  <property name="numDirections" type="int" value="4"/>
  <property name="rowstype" value="direction"/>
 </properties>
 <image source="sara.png" width="144" height="72"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="1" duration="100"/>
  </animation>
 </tile>
 <tile id="9">
  <animation>
   <frame tileid="9" duration="100"/>
   <frame tileid="10" duration="100"/>
   <frame tileid="11" duration="100"/>
   <frame tileid="10" duration="100"/>
  </animation>
 </tile>
 <tile id="18">
  <animation>
   <frame tileid="18" duration="100"/>
   <frame tileid="19" duration="100"/>
   <frame tileid="20" duration="100"/>
   <frame tileid="19" duration="100"/>
  </animation>
 </tile>
 <tile id="27">
  <animation>
   <frame tileid="27" duration="100"/>
   <frame tileid="28" duration="100"/>
   <frame tileid="29" duration="100"/>
   <frame tileid="28" duration="100"/>
  </animation>
 </tile>
</tileset>
